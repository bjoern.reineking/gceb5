# gceb5

The goal of gceb5 is to support the GCE B5 course "Global change impacts on species distributions"

## Installation

You can install the released version of gceb5 from [gitlab](https://gitlab.irstea.fr) with:

``` r
library(devtools)
devtools::install_git('https://gitlab.irstea.fr/bjoern.reineking/gceb5.git')
```

